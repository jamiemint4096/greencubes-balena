#! /bin/bash

ffmpeg -f concat -safe 0 -i <(ls `pwd`/**/timelapse.mp4 | awk '{print "file " $1}') -c copy out.mp4
